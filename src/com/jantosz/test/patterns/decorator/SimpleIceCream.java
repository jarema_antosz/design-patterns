package com.jantosz.test.patterns.decorator;

public class SimpleIceCream implements IceCream {

	@Override
	public String makeIceScream() {
		return "ordinary ice cream";
	}

}
