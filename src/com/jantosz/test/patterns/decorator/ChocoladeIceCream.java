package com.jantosz.test.patterns.decorator;

public class ChocoladeIceCream extends AbstractDecorator implements IceCream {

	public ChocoladeIceCream(IceCream iceCream) {
		super(iceCream);
	}

	@Override
	public String makeIceScream() {
		return wrappedIceCream.makeIceScream() + " chocolade";
	}

}
