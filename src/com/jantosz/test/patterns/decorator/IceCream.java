package com.jantosz.test.patterns.decorator;

public interface IceCream {
	
	String makeIceScream();

}
