package com.jantosz.test.patterns.decorator;

public class NutsIceCream extends AbstractDecorator implements IceCream {

	public NutsIceCream(IceCream iceCream) {
		super(iceCream);
	}

	@Override
	public String makeIceScream() {
		return wrappedIceCream.makeIceScream() + " nuts";
	}

}
