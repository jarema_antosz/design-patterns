package com.jantosz.test.patterns.decorator;

public class DecoratorTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String iceCream = new NutsIceCream(new ChocoladeIceCream(
				new SimpleIceCream())).makeIceScream();

		System.out.println(iceCream);

	}

}
