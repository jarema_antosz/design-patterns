package com.jantosz.test.patterns.decorator;

public abstract class AbstractDecorator implements IceCream {
	
	protected IceCream wrappedIceCream;
	
	public AbstractDecorator(IceCream iceCream){
		this.wrappedIceCream = iceCream;
	}

	@Override
	public String makeIceScream() {
		return wrappedIceCream.makeIceScream();
	}

}
