package com.jantosz.test.patterns.builder;

public class CarBuilderExample {

	public static void main(String[] args) {
		
		Car car = new Car.CarBuilder().addEngine(500).addDoors(5).setRadio(true).setAbs(false).addSeats(5).buildCar();
		
		System.out.println(car);

	}

}

class Car {

	private final int engine;
	private final int seats;
	private final int doors;
	private final boolean abs;
	private final boolean radio;

	@Override
	public String toString() {
		return "Car [engine=" + engine + ", seats=" + seats + ", doors="
				+ doors + ", abs=" + abs + ", radio=" + radio + "]";
	}
	
	private Car(CarBuilder builder){
		this.engine = builder.engine;
		this.seats = builder.seats;
		this.doors = builder.doors;
		this.abs = builder.abs;
		this.radio = builder.radio;
	}
	
	

	public static class CarBuilder {
		
		private int engine;
		private int seats;
		private int doors;
		private boolean abs;
		private boolean radio;
		
		CarBuilder addEngine(int engine){
			this.engine = engine;
			return this;
		}
		
		CarBuilder addSeats(int seats){
			this.seats = seats;
			return this;
		}
		
		CarBuilder addDoors(int doors){
			this.doors = doors;
			return this;
		}
		
		CarBuilder setAbs(boolean abs){
			this.abs = abs;
			return this;
		}
		
		CarBuilder setRadio(boolean radio){
			this.radio = radio;
			return this;
		}
		
		Car buildCar(){
			return new Car(this);
		}
	

	}

}
