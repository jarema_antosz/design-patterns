package com.jantosz.test.patterns.factory;

public class FactoryDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Shape shape = null;
		
		shape = ShapeFactory.createShape(ShapeType.LINE);
		shape.draw();
		
		shape = ShapeFactory.createShape(ShapeType.SQUARE);
		shape.draw();
		
		shape = ShapeFactory.createShape(ShapeType.CIRCLE);
		shape.draw();
		

	}

}
