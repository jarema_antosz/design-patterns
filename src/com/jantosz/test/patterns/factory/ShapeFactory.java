package com.jantosz.test.patterns.factory;

public class ShapeFactory {
	
	public static Shape createShape(ShapeType shapeType){
		
		Shape shape = null;
		switch (shapeType) {
		case LINE:
			shape =  new Line();
			break;

		case SQUARE:
			shape =  new Square();
			break;
		case CIRCLE:
			shape = new Circle();
			break;
		default:
			break;
		}
		
		return shape;
	}

}
