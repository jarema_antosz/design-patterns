package com.jantosz.test.patterns.factory;

public interface Shape {
	
	public void draw();

}
