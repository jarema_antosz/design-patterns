package com.jantosz.test.patterns.factory;

public enum ShapeType {
	LINE, SQUARE, CIRCLE

}
