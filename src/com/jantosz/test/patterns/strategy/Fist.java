package com.jantosz.test.patterns.strategy;

public class Fist implements IWeapon {

	@Override
	public void useWeapon() {
		System.out.println("job to the face");
	}

}
