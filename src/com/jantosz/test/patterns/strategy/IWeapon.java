package com.jantosz.test.patterns.strategy;

public interface IWeapon {
	
	public void useWeapon();

}
