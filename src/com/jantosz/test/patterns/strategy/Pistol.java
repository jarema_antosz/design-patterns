package com.jantosz.test.patterns.strategy;

public class Pistol implements IWeapon {

	@Override
	public void useWeapon() {
		System.out.println("shot");
	}

}
