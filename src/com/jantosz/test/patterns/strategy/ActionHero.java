package com.jantosz.test.patterns.strategy;

public class ActionHero {
	
	private IWeapon currentWeapon;
	
	public ActionHero(){
		currentWeapon = new Fist();
	}
	
	public void setWeapon(IWeapon weapon){
		this.currentWeapon = weapon;
	}
	
	public void attack(){
		currentWeapon.useWeapon();
	}

}
