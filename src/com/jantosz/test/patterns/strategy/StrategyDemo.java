package com.jantosz.test.patterns.strategy;

public class StrategyDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ActionHero hero = new ActionHero();
		hero.attack();
		
		hero.setWeapon(new Knife());
		hero.attack();
		
		hero.setWeapon(new Pistol());
		hero.attack();

	}

}
