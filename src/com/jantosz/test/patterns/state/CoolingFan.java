package com.jantosz.test.patterns.state;

public class CoolingFan {

	private State currentState;
	
	public CoolingFan(){
		this.currentState = new OffState();
	}

	public void setState(State state) {
		this.currentState = state;
	}

	public void pressSwitch() {
		currentState.pressSwitch(this);
	}

	public void makeSound() {
		// TODO Auto-generated method stub
		currentState.makeNoise();
	}

}
