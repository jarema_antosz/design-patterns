package com.jantosz.test.patterns.state;

public interface State {
	
	public void pressSwitch(CoolingFan coolingFan);
	public void makeNoise();

}
