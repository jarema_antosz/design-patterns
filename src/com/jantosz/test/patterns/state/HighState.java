package com.jantosz.test.patterns.state;

public class HighState implements State {

	@Override
	public void pressSwitch(CoolingFan coolingFan) {

		coolingFan.setState(new OffState());
		System.out.println("Turning off");
	}
	
	@Override
	public void makeNoise() {
		System.out.println("noisy bzzzzz");
		
	}

}
