package com.jantosz.test.patterns.state;

public class MediumState implements State {

	@Override
	public void pressSwitch(CoolingFan coolingFan) {
		
		coolingFan.setState(new HighState());
		System.out.println("running on high speed");

	}
	
	@Override
	public void makeNoise() {
		System.out.println("bzzzzz");
		
	}

}
