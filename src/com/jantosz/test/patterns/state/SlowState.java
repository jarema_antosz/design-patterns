package com.jantosz.test.patterns.state;

public class SlowState implements State {

	@Override
	public void pressSwitch(CoolingFan coolingFan) {

		coolingFan.setState(new MediumState());
		System.out.println("running on medium speed");

	}
	
	@Override
	public void makeNoise() {
		System.out.println("silent bzzzzz");
		
	}

}
