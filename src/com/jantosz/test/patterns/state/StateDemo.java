package com.jantosz.test.patterns.state;

public class StateDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CoolingFan fan = new CoolingFan();
		
		while(true){
			fan.pressSwitch();
			fan.makeSound();
		}

	}

}
