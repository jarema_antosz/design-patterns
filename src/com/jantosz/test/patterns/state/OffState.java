package com.jantosz.test.patterns.state;

public class OffState implements State {

	@Override
	public void pressSwitch(CoolingFan coolingFan) {

		coolingFan.setState(new SlowState());
		System.out.println("running on slow speed");

	}

	@Override
	public void makeNoise() {
		System.out.println("........................");
		
	}

}
