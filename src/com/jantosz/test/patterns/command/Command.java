package com.jantosz.test.patterns.command;

public interface Command {
	
	public void execute();

}
