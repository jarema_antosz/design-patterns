package com.jantosz.test.patterns.command;

public class Switch {
	
	private Command upCommand;
	private Command downCommand;
	
	
	
	public Switch(Command upCommand, Command downCommand) {
		super();
		this.upCommand = upCommand;
		this.downCommand = downCommand;
	}

	public void flipUp(){
		upCommand.execute();
	}
	
	public void flipDown(){
		downCommand.execute();
	}

}
